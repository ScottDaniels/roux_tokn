
/*
	Abstract:	Basic tokeniser.

	Date:		30 December 2020
	Author:		E. Scott Daniels
*/

/*!
	The basic tokeniser splits a constant string on a single separator
	character. The separator may be escaped in the string using either
	the default, a back slant, or the user programme may supply a custom
	escape character.
*/


/**
	The basic tokenise function is a simple parser which will split the
	given buffer into tokens based on the control (ctl) string which contains
	the separater and escap characters. 

	The control string is a one or two character string which defines:

	- The separator character
	- The escape character

	The escape character is optional and defaults to a back slant when
	the string contains only the separater character (e.g. "," and ",\\"
	are the same.

	The return is a vector of strings where each string is a token.

	# Example
	The following example illustrates how to use the basic tokeniser to
	split a comma separated list of tokens into a vector. The middle token
	contains a comma which is escaped with the non-default escape character
	(a carrot).

	```
		use roux_tokn::basic as tokn;

		let data = "Stand Up and Cheer,Paul P. McNeely^,1909,Ohio U.";
		let expect = vec![ "Stand Up and Cheer", "Paul P. McNeely,1909", "Ohio U." ];
		let tokens = tokn::tokenise( data, ",^" );
	
		let mut errs = 0;
		for i in 0..tokens.len() {
			if i < expect.len() {
				if tokens[i] != expect[i] {
					println!( "token[{}] was not the expected string found '{}' while expecting '{}'", i, tokens[i], expect[i]  );
					errs += 1;
				}
			} else {
				println!( "too many tokens" );
				errs += 1;
				break;
			}
		}

		# assert!( errs == 0 );		// catch bug in doc/test, but don't show
	```
*/
pub fn tokenise( buf: &str, ctl: &str ) -> Vec<String> {
	return parse( buf, ctl, true );
}

/**
	The tokenise_skip function is a basic tokenising parser which
	does **not** generate nil (empty) tokens when consecutive separators
	appeear in the input.  This is likely not what is desired when
	tokenising comma separated data, but might be the expected
	behaviour when parsing space separated tokens where multiple
	spaces should be treated as a single separator. 

	The control string is a one or two character string which defines:

	- The separator character
	- The escape character

	The escape character is optional and defaults to a back slant when
	the string contains only the separater character (e.g. "," and ",\\"
	are the same.

	The return is a vector of strings where each string is a token.

	# Example
	The following example illustrates how the *tokenise_skip* function
	skips multiple adjacent occurences of the separater character to
	effectivly trim leading and trailing separaters as well as multiple
	spaces betwen words.

	```
		use roux_tokn::basic as tokn;

		let data = "    Rambling Wreck from    Georga Tech!   ";
		let expect = vec![ "Rambling", "Wreck", "from", "Georga", "Tech!" ];
		let tokens = tokn::tokenise_skip( data, " " );
	
		let mut errs = 0;
		for i in 0..tokens.len() {
			if i < expect.len() {
				if tokens[i] != expect[i] {
					println!( "token[{}] was not the expected string found '{}' while expecting '{}'", i, tokens[i], expect[i]  );
					errs += 1;
				}
			} else {
				println!( "too many tokens" );
				errs += 1;
				break;
			}
		}

		# assert!( errs == 0 );		// catch bug in doc/test, but don't show
*/
pub fn tokenise_skip( buf: &str, ctl: &str ) -> Vec<String> {
	return parse( buf, ctl, false );
}

/*
	The real parser. If *nil_tokens* is true, then multiple
	adjacent separator characters result in nil (empty) tokens.
	This is probalby the expected behaviour when a comma, or 
	vertical bar is used as the separator, but probably not
	what is desired when parsing white space separated tokens.

	Note: the multi separator parser is better suited for a true
	white space parser.
*/
fn parse( buf: &str, ctl: &str, nil_tokens: bool )  -> Vec< String > {
	let cv: Vec<char> = String::from( ctl ).chars().collect();
	let sch = cv[0];
	let mut ech: char  = '\\';
	if cv.len() > 1 {
		ech = cv[1];
	}

	let wbuf: Vec<char> = String::from( buf ).chars().collect();

	let mut rv: Vec<String> = Vec::new();		// return vector of tokens
	let mut tok: Vec<char> = Vec::new();		// current token being built
	let mut force = false;						// set to true if we blindly put the next char in w/o checking

	for c in wbuf {
		if force {
			tok.push( c );
			force = false;
		} else {
			if c == ech {
				force = true;
			} else {
				if c == sch {
					if nil_tokens || tok.len() > 0 {
						let ts: String = tok.into_iter().collect();
						rv.push( ts );
						tok = Vec::new();
					}
				} else {
					tok.push( c );
				}
			}
		}
	}

	if nil_tokens || tok.len() > 0 {
		let ts: String = tok.into_iter().collect();
		rv.push( ts );
	}

	return rv;
}
