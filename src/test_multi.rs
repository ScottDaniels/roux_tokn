/*
	Abstract:	These are the unit tests for the multi module.
	Date:		14 April 2021
	Author:		E. Scott Daniels
*/

#[cfg(test)]
mod tests {
	use crate::multi as multi;

	const TRUE: bool = true;
	const FALSE: bool = false;

    #[test]
    fn multi() {
		let input = "192.168.1.230:84306";
		let tokens = multi::tokenise( input, ".:", '\\', '"', FALSE );
		let expect = vec![ "192", "168", "1", "230", "84306" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi: [{}] {}", i, tokens[i] );
			if i < expect.len()  {
				assert!( expect[i] == tokens[i], "token [{}] didn't match expected token: {}", tokens[i], expect[i] );
			}
		}
		assert!( tokens.len() == expect.len(), "number of tokens generated ({}) didn't match expected number ({})", tokens.len(), expect.len() );
    }

    #[test]
    fn multi_quoted() {
		let input = "192.168.\"1.230\":84306";
		let tokens = multi::tokenise( input, ".:", '\\', '"', FALSE );

		assert_eq!( tokens.len(), 4, "lengths are the same (not expected)" );
		let mut i = 0;
		for t in tokens {
			eprintln!( "<TEST> multi-quote: [{}] {}", i, t );
			i += 1;
		}
    }

    #[test]
    fn multi_escaped() {
		let input = "192.168.1\\.230:84306";
		let tokens = multi::tokenise( input, ".:", '\\', '"', FALSE );

		assert_eq!( tokens.len(), 4, "lengths are the same (not expected)" );
		assert_eq!( tokens[0], "192", "token 0 bad" );
		assert_eq!( tokens[1], "168", "token 1 bad" );
		assert_eq!( tokens[2], "1.230", "token 2 bad" );
		assert_eq!( tokens[3], "84306", "token 3 bad" );

		let mut i = 0;
		for t in tokens {
			eprintln!( "<TEST> multi-esc: [{}] {}", i, t );
			i += 1;
		}
    }

    #[test]
    fn multi_v6() {
		let input = "[fe80::fc84:6fff:fecf:3540]:84306";
		let tokens = multi::tokenise( input, "[].", '\\', '"', FALSE );

		assert_eq!( tokens.len(), 2, "unexpected number of tokens" );
		assert_eq!( tokens[0], "fe80::fc84:6fff:fecf:3540", "token 1 bad" );
		assert_eq!( tokens[1], ":84306", "token 2 bad" );
		let mut i = 0;
		for t in tokens {
			eprintln!( "<TEST> multi-v6: [{}] {}", i, t );
			i += 1;
		}
    }

	/*
		Multi seperators with escaped things in the quoted portions.
	*/
    #[test]
    fn multi_qesc() {
		let input = "area = height * width; volume = height * width * depth; \"# \\\"\\$area\\\" and \\\"\\$volume\\\"\"";
		let expect = vec![ "area ", "=", " height ", "*", " width", ";", " volume ", "=", " height ", "*", " width ", "*", " depth", ";", " # \"\\$area\" and \"\\$volume\"" ];

		let tokens = multi::tokenise( input, "+-/*;=", '\\', '"', TRUE );

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-quote: [{}] ({})", i, tokens[i] );
			if i < expect.len()  {
				assert!( tokens[i] == expect[i], "bad token at [{}]: ({})", i, tokens[i] );
			}
		}
    }

    #[test]
    fn multi_keep() {
		let input = "[fe80::fc84:6fff:fecf:3540]:84306";
		let tokens = multi::tokenise( input, "[].", '\\', '"', TRUE );		// tokenise and keep the separator in the result

		let expect = vec![ "[", "fe80::fc84:6fff:fecf:3540", "]", ":84306" ];		// there is a lead empty token as char 0 is a sep

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-keep: [{}] {}", i, tokens[i] );
		}

		assert!( expect.len() == tokens.len(), "tokens len ({} didn't match expected number ({})", tokens.len(), expect.len() );
		for i in 0..tokens.len() {
			assert!( expect[i] == tokens[i], "token[{}] did not match expected: ({}) != ({})", i, tokens[i], expect[i] );
		}
    }


	/*
		ensure that the dangling flag is set as expected
	*/
    #[test]
    fn multi_dq() {
		let input = "{ \"performer\": \"The Marching 110 from Ohio University },";		// dangling quote
		let (tokens,dq) = multi::tokenise_dq( input, "{}:, ", '\\', '"', FALSE, FALSE );		// tokenise without keeping seps

		let expect = vec![ "performer", "The Marching 110 from Ohio University }," ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-keep: [{}] {}", i, tokens[i] );
		}

		assert!( expect.len() == tokens.len(), "tokens len ({} didn't match expected number ({})", tokens.len(), expect.len() );
		for i in 0..tokens.len() {
			assert!( expect[i] == tokens[i], "token[{}] did not match expected: ({}) != ({})", i, tokens[i], expect[i] );
		}
		assert!( dq, "dangling quote flage was NOT set when expected" );
    }

	/*
		ensure that the dangling flag is not set when there is no dangling quote.
	*/
    #[test]
    fn multi_nodq() {
		let input = "{ \"performer\": \"The Marching 110 from Ohio University\" },";			// no dangling quote
		let (tokens,dq) = multi::tokenise_dq( input, "{}:, ", '\\', '"', FALSE, FALSE );		// tokenise without keeping seps

		let expect = vec![ "performer", "The Marching 110 from Ohio University" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-nodq: [{}] {}", i, tokens[i] );
		}

		assert!( expect.len() == tokens.len(), "tokens len ({} didn't match expected number ({})", tokens.len(), expect.len() );
		for i in 0..tokens.len() {
			assert!( expect[i] == tokens[i], "token[{}] did not match expected: ({}) != ({})", i, tokens[i], expect[i] );
		}
		assert!( !dq, "dangling quote flage was set when NOT expected" );
    }

	/*
		Test with quote in progress flag set.
	*/
    #[test]
    fn multi_pqip() {
		let input = "performer\": \"The Marching 110 from Ohio University\" },";			// no dangling quote
		let (tokens,dq) = multi::tokenise_dq( input, "{}:, ", '\\', '"', FALSE, TRUE );		// tokenise without keeping seps

		let expect = vec![ "performer", "The Marching 110 from Ohio University" ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-pqip: [{}] {}", i, tokens[i] );
		}

		assert!( expect.len() == tokens.len(), "tokens len ({} didn't match expected number ({})", tokens.len(), expect.len() );
		for i in 0..tokens.len() {
			assert!( expect[i] == tokens[i], "token[{}] did not match expected: ({}) != ({})", i, tokens[i], expect[i] );
		}
		assert!( !dq, "dangling quote flage was set when NOT expected" );
    }

	/*
		In multi parsing we don't "squish" nil tokens which makes something like this difficult if seps are whitespace and 
		commas:
			"foo", "", "bar"

		The middle token appears to be whitespace.  The multi parser will replace "" with \nil (assuming \ is the scape
		character).  Test this.

		We use the carrot (^) as the escape character to make the code slightly less confusing.
	*/
    #[test]
    fn multi_nil() {
		let input = "'Bush Hall', '', 'Biddle Hall', ''";
		let (tokens,dq) = multi::tokenise_dq( input, ", ", '^', '\'', FALSE, FALSE );		// tokenise without keeping seps

		let expect = vec![ "Bush Hall", "^nil", "Biddle Hall", "^nil"  ];

		for i in 0..tokens.len() {
			eprintln!( "<TEST> multi-nil: [{}] {}", i, tokens[i] );
		}

		assert!( expect.len() == tokens.len(), "tokens len ({} didn't match expected number ({})", tokens.len(), expect.len() );
		for i in 0..tokens.len() {
			assert!( expect[i] == tokens[i], "token[{}] did not match expected: ({}) != ({})", i, tokens[i], expect[i] );
		}
		assert!( !dq, "dangling quote flage was set when NOT expected" );
    }
}
